// custom javascript

const data = {};

let myChart;

$(document).ready(function() {
  console.log('Sanity Check!');

  $('.loader-wrapper').addClass('is-active');
  $('#last-updated').hide();

  // get all data
  getData();
});

function changeChart() {
  const country = document.getElementById('select-country').value;
  const type = document.getElementById('select-type').value;
  const ctx = document.getElementById('my-chart').getContext('2d');
  const temp = getConfig(country, type);

  if (myChart) { myChart.destroy(); }

  myChart = new Chart(ctx, temp);
}

function getConfig(country, type){
  if (type === 'null'){
    type = 'line';
  }

  if (country === 'null') {
    country = 'United States';
  }

  return {
    // The type of chart we want to create
    type: type,

    // The data for our dataset
    data: {
      labels: data[country][0],
      datasets: [{
        label: `Total Cases In ${country}`,
        backgroundColor: 'rgb(255, 99, 132)',
        borderColor: 'rgb(255, 99, 132)',
        data: data[country][1]
      }]
    },

    // Configuration options go here
    options: {
      responsive: true,
      scales: {
        xAxes: [
          {
            ticks:{
              display: true,
              autoSkip: true,
              maxTicksLimit: 5
            }
          }
        ],
      }
    }
  }
}

function getData() {
  $.ajax({
    url: '/api/coronavirus/countries/cases',
  })
  .done((res) => {
    // add table data
    const summaries = res.summary;
    let table = $('.table').DataTable({
      columns: [
        {data: 'country' },
        {data: 'total_cases', render: $.fn.dataTable.render.number(',', '.') },
        {data: 'new_cases_today', render: $.fn.dataTable.render.number(',', '.') },
        {data: 'new_cases_yesterday', render: $.fn.dataTable.render.number(',', '.') },
        {data: 'percent_change' },
        {data: 'prediction', render: $.fn.dataTable.render.number(',', '.') },
      ],
      order: [[ 1, 'desc' ]],
      paging: false,
      fixedHeader: true,
    });
    table.rows.add(summaries).draw();

    const countries = document.getElementById('select-country');
    const startDate = new Date('3/1/2020');  // only show dates >= 3/2/20

    for (const item of summaries) {
        const el = document.createElement('option');
        el.textContent = item['country'];
        el.value = item['country'];
        countries.appendChild(el);

        const date = [];
        const newCases = [];
        date_obj = item['dates'][0];

        for (const key in date_obj) {
          if (new Date(key) >= startDate) {
            date.push(key);
            newCases.push(date_obj[key]);
          }
        }

        data[item.country] = [date, newCases];
    }

    const ctx = document.getElementById('my-chart').getContext('2d');
    myChart = new Chart(ctx, getConfig('United States', 'line'));

    // sum totals
    const column_total_cases = table.column(1);
    const total_cases_sum = column_total_cases.data().reduce((a, b) => Number(a) + Number(b));
    $(column_total_cases.footer()).html(total_cases_sum);
    const column_today_sum = table.column(2);
    const total_today_sum = column_today_sum.data().reduce((a, b) => Number(a) + Number(b));
    $(column_today_sum.footer()).html(total_today_sum);
    const column_yesterday_sum = table.column(3);
    const total_yesterday_sum = column_yesterday_sum.data().reduce((a, b) => Number(a) + Number(b));
    $(column_yesterday_sum.footer()).html(total_yesterday_sum);
    const column_prediction_sum = table.column(5);
    const total_prediction_sum = column_prediction_sum.data().reduce((a, b) => Number(a) + Number(b));
    $(column_prediction_sum.footer()).html(total_prediction_sum);

    // add last updated
    $('#last-updated-date').html(new Date(res.last_updated));
    $('.dataTables_filter input[type="search"]').addClass('input is-small').css('width', 'auto');
  })
  .fail((err) => {
    console.log(err);
  })
  .always(() => {
    $('#last-updated').show();
    $('.loader-wrapper').removeClass('is-active');
  })
}
