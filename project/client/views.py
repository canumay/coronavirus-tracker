from flask import Blueprint, render_template

core_blueprint = Blueprint("core", __name__)


@core_blueprint.route("/")
def home():
    return render_template("home.html")
