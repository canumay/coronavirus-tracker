# Coronavirus Cases Tracker Vue.js Implementation

### How to activate Vue based dashboard?

Make following changes in `project/__init__.py`:

Change 17-20 lines.
``` 
app = Flask( __name__, template_folder="./client_vue/dist/", static_folder="./client_vue/dist/")
```

Change 31. line.
```
from project.client_vue.views import core_blueprint
```

### Development

```
npm run serve
```

### Build

```
npm run build
```