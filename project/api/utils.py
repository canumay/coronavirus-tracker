# project/api/utils.py

import csv
from datetime import datetime

import requests
from cachetools import TTLCache, cached


@cached(cache=TTLCache(maxsize=1024, ttl=7200))
def get_data(url):
    """
    Retrieves data, transforms it for consumption, and then caches it for 2 hours.
    """

    data = []

    with requests.get(url, stream=True) as request:
        lines = (line.decode("utf-8") for line in request.iter_lines())
        csv_reader = csv.reader(lines)
        headers = next(csv_reader)
        for row in csv_reader:
            data.append(row)

    # import os
    # from pathlib import Path

    # BASE_DIR = Path(__file__).resolve(strict=True).parent
    # csv_file = os.path.join(BASE_DIR, "time_series_covid19_confirmed_global.csv")
    # with open(csv_file, "rt") as f:
    #     csv_reader = csv.reader(f)
    #     headers = next(csv_reader)
    #     for row in csv_reader:
    #         data.append(row)

    return (headers, data, datetime.utcnow().isoformat() + "Z")


def calculate_totals(data):
    countries = {}
    country_names = []

    for region in data:
        country = get_standard_country_name(region[1])
        current_totals = region[4:]
        if country in countries:
            totals = countries[country]
            new_totals = [int(x) + int(y) for x, y in zip(current_totals, totals)]
            countries[country] = new_totals
        else:
            countries[country] = current_totals
            country_names.append(country)
    return (countries, country_names)


def get_summary(country_data, headers, summary_type):
    summary_data = []
    percent_change = 0

    for key, val in country_data.items():
        # get dates
        dates = []
        dictionary = dict(zip(headers[4:], val))
        dates.append(dictionary)

        # summarize data
        total_today = int(val[-1])
        total_yesterday = int(val[-2])
        new_today = total_today - total_yesterday
        new_yesterday = total_yesterday - int(val[-3])
        total_past_three_days = (
            (total_today - total_yesterday)
            + (total_yesterday - int(val[-3]))
            + (int(val[-3]) - int(val[-4]))
        )
        percent_change = (
            ((new_today - new_yesterday) / new_yesterday) if new_yesterday else 0.00
        ) * 100
        prediction = round((total_past_three_days / 3), 0)

        summary_data.append(
            {
                "country": key,
                f"total_{summary_type}": total_today,
                f"new_{summary_type}_today": new_today,
                f"new_{summary_type}_yesterday": new_yesterday,
                "percent_change": round(percent_change, 0),
                "prediction": prediction if prediction >= 0 else 0,
                "dates": dates,
            }
        )

    return summary_data


def get_province_data(headers, raw_data, summary_type):
    province_summary_data = []
    province_keys = []
    ignore_keys = ["Grand Princess", "Recovered", "Diamond Princess"]
    for row in raw_data:
        if row[1] == "Canada" and row[0] not in ignore_keys:
            dates = []
            dictionary = dict(zip(headers[4:], row[4:]))
            dates.append(dictionary)

            province_keys.append(row[0])
            total_today = int(row[-1])
            total_yesterday = int(row[-2])
            new_today = total_today - total_yesterday
            new_yesterday = total_yesterday - int(row[-3])
            total_past_three_days = (
                (total_today - total_yesterday)
                + (total_yesterday - int(row[-3]))
                + (int(row[-3]) - int(row[-4]))
            )
            percent_change = (
                ((new_today - new_yesterday) / new_yesterday) if new_yesterday else 0.00
            ) * 100
            prediction = round((total_past_three_days / 3), 0)

            province_summary_data.append(
                {
                    "province": row[0],
                    f"total_{summary_type}": total_today,
                    f"new_{summary_type}_today": new_today,
                    f"new_{summary_type}_yesterday": new_yesterday,
                    "percent_change": round(percent_change, 0),
                    "prediction": prediction if prediction >= 0 else 0,
                    "dates": dates,
                }
            )

    return province_summary_data, province_keys


def get_state_data(raw_data, isDeaths):
    state_summary_data = {}
    for row in raw_data:
        state = row[1]
        curr_date = row[0]
        cases = int(row[3])
        deaths = int(row[4])
        if isDeaths:
            label_value = "deaths"
            value = deaths
        else:
            label_value = "cases"
            value = cases
        if state not in state_summary_data:
            date_object = []
            date_object.append({curr_date: value})
            state_summary_data[state] = {
                "state": state,
                f"total_{label_value}": value,
                f"new_{label_value}_today": value,
                f"new_{label_value}_yesterday": 0,
                "percent_change": 0.0,
                "prediction": 0.0,
                "dates": date_object,
            }
        else:
            state_data = state_summary_data[state]
            new_value_yesterday = state_data[f"new_{label_value}_today"]
            new_value_today = value - state_data[f"total_{label_value}"]
            percent_change = (
                ((new_value_today - new_value_yesterday) / new_value_yesterday)
                if new_value_yesterday
                else 0.00
            ) * 100
            keys_list = list(state_data["dates"][0].keys())
            prediction = 0
            if len(keys_list) > 3:
                total_past_three_days = (
                    (value - state_data["dates"][0][keys_list[-1]])
                    + (
                        state_data["dates"][0][keys_list[-1]]
                        - state_data["dates"][0][keys_list[-2]]
                    )
                    + (
                        state_data["dates"][0][keys_list[-2]]
                        - state_data["dates"][0][keys_list[-3]]
                    )
                )
                prediction = round((total_past_three_days / 3), 0)

            state_data[f"total_{label_value}"] = value
            state_data[f"new_{label_value}_today"] = new_value_today
            state_data[f"new_{label_value}_yesterday"] = new_value_yesterday
            state_data["percent_change"] = round(percent_change, 0)
            state_data["prediction"] = prediction if prediction >= 0 else 0
            state_data["dates"][0][curr_date] = value

    return state_summary_data


def get_standard_model(data):
    model = []
    states = []
    for key in data.keys():
        model.append(data[key])
        states.append(key)

    return model, states


def get_summary_latest(data):
    total = 0
    for row in data:
        total += int(row[-1])
    return total


def get_standard_country_name(country):
    """
    A utility function that takes standardizes the country naming
    """

    if country == "Korea, South":
        country = "South Korea"
    if country == "Taiwan*":
        country = "Taiwan"
    if country == "US":
        country = "United States"

    return country
