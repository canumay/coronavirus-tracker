# Coronavirus Tracker API and Dashboard

[![pipeline status](https://gitlab.com/monitaur.ai/coronavirus-tracker/badges/master/pipeline.svg)](https://gitlab.com/monitaur.ai/coronavirus-tracker/commits/master)

Tracking the development of the Novel Coronavirus COVID-19.

- Dashboard: https://covid.monitaur.ai
- API: https://covid.monitaur.ai/api/coronavirus/summary

## Getting Started

### With Docker

```sh
$ docker-compose up -d --build
```

### Without Docker

Create and activate a virtual environment, install the requirements, set the environment variables, and then run the server:

```sh
(source)$ pip install -r requirements-dev.txt
(source)$ export FLASK_ENV=development
(source)$ export APP_SETTINGS=project.config.DevelopmentConfig
(source)$ python manage.py run -p 5003
```

### Sanity check

1. [http://localhost:5003/](http://localhost:5003/)
1. [http://localhost:5003/api/ping](http://localhost:5003/api/ping)
1. [http://localhost:5003/doc/](http://localhost:5003/doc/)
1. [http://localhost:5003/api/coronavirus](http://localhost:5003/api/coronavirus)

1. [http://localhost:5003/api/coronavirus/countries/cases](http://localhost:5003/api/coronavirus/countries/cases)
1. [http://localhost:5003/api/coronavirus/countries/deaths](http://localhost:5003/api/coronavirus/countries/deaths)
1. [http://localhost:5003/api/coronavirus/states/cases](http://localhost:5003/api/coronavirus/states/cases)
1. [http://localhost:5003/api/coronavirus/states/deaths](http://localhost:5003/api/coronavirus/states/deaths)
1. [http://localhost:5003/api/coronavirus/provinces/cases](http://localhost:5003/api/coronavirus/provinces/cases)
1. [http://localhost:5003/api/coronavirus/provinces/deaths](http://localhost:5003/api/coronavirus/provinces/deaths)
1. [http://localhost:5003/api/coronavirus/summary](http://localhost:5003/api/coronavirus/summary)

## Example API Data

`api/coronavirus/countries/cases`:

```json
{
  "country": "United States",
  "cases_total": 121478,
  "cases_new_today": 19821,
  "cases_new_yesterday": 17821,
  "percent_change": 11,
  "prediction": 18567,
  "dates": [
    {
      "3/28/20": 121478,
      "3/29/20": 140886
    }
  ]
}
```

`api/coronavirus/countries/deaths`:

```json
{
  "country": "United States",
  "cases_total": 2467,
  "cases_new_today": 441,
  "cases_new_yesterday": 445,
  "percent_change": -1,
  "prediction": 419,
  "dates": [
    {
      "3/28/20": 2026,
      "3/29/20": 2467
    }
  ]
}
```

## Test

Run the tests:

```sh
$ docker-compose exec web python -m pytest "project/tests" -p no:warnings --cov="project"
```

Lint:

```sh
$ docker-compose exec web flake8 project
```

Run Black and isort:

```sh
$ docker-compose exec web black project
$ docker-compose exec web /bin/sh -c "isort project/**/*.py"
```

## Data

Data sourced from:

1. country and province data - [2019 Novel Coronavirus COVID-19 (2019-nCoV) Data Repository by Johns Hopkins CSSE](https://github.com/CSSEGISandData/COVID-19).
1. state data - [Coronavirus (Covid-19) Data in the United States](https://github.com/nytimes/covid-19-data)

Data is transformed and then cached for 2 hours.
